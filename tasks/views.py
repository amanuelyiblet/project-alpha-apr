from django.shortcuts import render, redirect
from .forms import TaskForm
from django.contrib.auth.decorators import login_required
from .models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/projects/")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def task_list(request):
    list_of_tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": list_of_tasks}
    return render(request, "tasks/task_list.html", context)
