from django.urls import path
from .views import projects_list, project_details, create_project

urlpatterns = [
    path("", projects_list, name="list_projects"),
    path("project/<int:id>/", project_details, name="show_project"),
    path("create/", create_project, name="create_project"),
]
